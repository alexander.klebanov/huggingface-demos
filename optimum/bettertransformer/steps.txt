# p3.2xlarge Amazon EC2 instance
# Ubuntu 20.04

sudo apt-get update
sudo apt-get install python3-pip -y
pip install pip --upgrade
export PATH=/home/ubuntu/.local/bin:$PATH
pip install virtualenv

virtualenv bt
source bt/bin/activate
pip install -r requirements.txt